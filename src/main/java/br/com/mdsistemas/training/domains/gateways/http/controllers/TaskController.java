package br.com.mdsistemas.training.domains.gateways.http.controllers;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.domains.gateways.http.controllers.json.GetTaskListResponseJson;
import br.com.mdsistemas.training.domains.gateways.http.controllers.json.TaskJsonRequest;
import br.com.mdsistemas.training.domains.gateways.http.controllers.json.DeleteTaskJsonRequest;
import br.com.mdsistemas.training.domains.gateways.http.controllers.json.AlterTaskJsonRequest;
import br.com.mdsistemas.training.domains.gateways.http.controllers.json.TaskJsonResponse;
import br.com.mdsistemas.training.usecases.CreateTaskUseCase;
import br.com.mdsistemas.training.usecases.DeleteTaskUseCase;
import br.com.mdsistemas.training.usecases.LoadTaskUseCase;
import br.com.mdsistemas.training.usecases.AlterTaskUseCase;
import lombok.extern.slf4j.Slf4j;

@Validated
@Slf4j 
@CrossOrigin
@RestController
//@RequestMapping("/training/v1/tasks")
@RequestMapping("${baseurl.v1}/tasks")
public class TaskController { 
	
	private final static String ID_TASK = "ID_TASK";
	
	@Autowired
	private CreateTaskUseCase createTaskUseCase;
	
	@Autowired
	private LoadTaskUseCase LoadTaskUseCase;
	
	@Autowired
	private DeleteTaskUseCase DeleteTaskUseCase;
	
	@Autowired
	private AlterTaskUseCase AlterTaskUseCase;

	

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public TaskJsonResponse create(
			final @RequestBody(required = true) @NotNull @Valid TaskJsonRequest taskJsonRequest) {
		log.trace("taskJsonRequest: {}", taskJsonRequest);
		
		final Task taskToBeCreate = new Task(taskJsonRequest.getDescription(), taskJsonRequest.getDate());
		
		final Task taskCreated = this.createTaskUseCase.create(taskToBeCreate);
		
		final TaskJsonResponse response = new TaskJsonResponse(taskCreated.getId(), taskCreated.getDescription(), taskCreated.getDate(), taskCreated.isDone());
		
		return response;
	}
	
	
	@ResponseStatus(HttpStatus.OK)
	@GetMapping
	public GetTaskListResponseJson load() {
		final List<Task> taskList = this.LoadTaskUseCase.load();
		final List<TaskJsonResponse> taskJsonList = taskList.stream().map(u -> mapperUserResponseJsonFromUser(u)).collect(Collectors.toList());
		final GetTaskListResponseJson response = setResponseGetTaskList(taskJsonList);
		return response;
	}
	
	
	@ResponseStatus(HttpStatus.OK)
	@PostMapping("delete")
	public void delete (
			final @RequestBody(required = true) @NotNull @Valid DeleteTaskJsonRequest DeleteTaskJsonRequest){
		
		log.trace("DeleteTaskJsonRequest: {}", DeleteTaskJsonRequest);
		
		System.out.println("chamou"+DeleteTaskJsonRequest.getId_task());
		
		this.DeleteTaskUseCase.DeleteTaskById(DeleteTaskJsonRequest.getId_task());
	}
	
	@ResponseStatus(HttpStatus.OK)
	@PostMapping("alter")
	public void alter(
			final @RequestBody(required = true) @NotNull @Valid AlterTaskJsonRequest AlterTaskJsonRequest) {
		
		log.trace("AlterTaskJsonRequest: {}", AlterTaskJsonRequest);
		
		// final Task taskSearch = this.AlterTaskUseCase.search("");
		// System.out.println(AlterTaskJsonRequest.getId());
		this.AlterTaskUseCase.alterTask(AlterTaskJsonRequest.getId(),AlterTaskJsonRequest.getDescription(),AlterTaskJsonRequest.getDate(),AlterTaskJsonRequest.isDone());
	}
	
	private TaskJsonResponse mapperUserResponseJsonFromUser(final Task task) {
		
		final TaskJsonResponse taskJson = new TaskJsonResponse(task.getId(), task.getDescription(), task.getDate(), task.isDone());
			
		return taskJson;
		
	}
	
	private GetTaskListResponseJson setResponseGetTaskList(final List<TaskJsonResponse> taskJsonList) {
		
		final GetTaskListResponseJson response = new GetTaskListResponseJson();
		response.setTaskJsonResponse(taskJsonList);
		
		return response;
	}
	
	
	
	
	

}
