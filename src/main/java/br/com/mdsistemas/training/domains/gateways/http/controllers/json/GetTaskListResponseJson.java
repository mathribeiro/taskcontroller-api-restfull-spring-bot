package br.com.mdsistemas.training.domains.gateways.http.controllers.json;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetTaskListResponseJson {
	private List <TaskJsonResponse> TaskJsonResponse;
}
