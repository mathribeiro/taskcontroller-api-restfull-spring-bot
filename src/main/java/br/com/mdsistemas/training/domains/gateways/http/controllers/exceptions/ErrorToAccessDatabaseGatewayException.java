package br.com.mdsistemas.training.domains.gateways.http.controllers.exceptions;


import br.com.mdsistemas.sca.util.exceptionshandler.exceptions.ExceptionId;
import br.com.mdsistemas.sca.util.exceptionshandler.exceptions.GatewayException;

public class ErrorToAccessDatabaseGatewayException extends GatewayException {
	
	private static final long serialVersionUID = 6534031883452981854L;

	public ErrorToAccessDatabaseGatewayException() {
		super(ExceptionId.create("training.errorToAccessDatabase", "Error to access database."));
	}
}
