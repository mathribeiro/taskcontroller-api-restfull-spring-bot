package br.com.mdsistemas.training.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mdsistemas.training.domains.gateways.http.controllers.database.task.TaskDatabaseGateway;

@Service
public class DeleteTaskUseCase {
	
	@Autowired
	private TaskDatabaseGateway taskDatabaseGateway;
	
	public void DeleteTaskById(final String id) {
		this.taskDatabaseGateway.delete(id);
	}
}
