package br.com.mdsistemas.training.usecases;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.domains.gateways.http.controllers.database.task.TaskDatabaseGateway;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoadTaskUseCase {
	
	@Autowired
	private TaskDatabaseGateway taskDatabaseGateway;
	
	public List<Task> load() {
		//log.trace("task: {}",task);
		
		final List<Task> taskList = this.taskDatabaseGateway.loadTask();
		return taskList;
	}
	

	

	

}
