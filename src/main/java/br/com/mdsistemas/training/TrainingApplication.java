package br.com.mdsistemas.training;


import java.time.LocalDate;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.domains.gateways.http.controllers.database.task.mongo.repository.TaskRepository;


@SpringBootApplication
public class TrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingApplication.class, args);
	}
	
	/*
	@Bean 
	public String app(TaskRepository repositorio) {
		
		Scanner sc = new Scanner(System.in);
		
		short opc = 0;
		String id;
		
		do {
			opc = menu();
			
			switch (opc) {
				case 1:
					listaTasks(repositorio);
					break;
				case 2:
					System.out.println("Digita o ID da Task que deseja excluir:");
					id = sc.nextLine();
					excluirPorId(repositorio,id);
					break;
				case 3:
					System.out.println("Digita o ID da Task que deseja alterar o status para 'true':");
					id = sc.nextLine();
					alterarStatusPorId(repositorio,id);
					break;
				case 4:
					System.out.println("Digita a descricao da Task:");
					String desc = sc.nextLine();
					System.out.println("Digita a data da Task:");
					String data = sc.nextLine();
					
					cadastraTask(repositorio, desc, data);
					break;
				default: 
					System.out.println("Programa Encerrado!");
			}
			
		} while (opc <= 4);
				
		return "";
	}
	*/

	
	private short menu() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("\n1- Listar todas as Tasks\n"
				+ "2- Excluir uma Task pelo ID\n"
				+ "3- Alterar status de uma Task\n"
				+ "4- Cadastrar nova Task\n\n"
				+ "Digita sua opção desejada:");
		
		return sc.nextShort();
		
	}
	
	private void listaTasks(TaskRepository repositorio) {
		
		System.out.println("\nTasks Cadastrados no Banco:\n");
		
		for (Task task: repositorio.findAll()) {
			System.out.println(task);
		}
		
		// System.out.println(repositorio.findAll());
				
	}

	
	private void excluirPorId(TaskRepository repositorio, String id) {
		if (repositorio.countById(id) > 0) {
			repositorio.deleteById(id);
			System.out.println("\nTask excluida com sucesso!\n");
		} else {
			System.out.println("\nNao foi encontrado nenhuma Task com esse ID!\n");
		}
		
	}
	
	
	private void alterarStatusPorId(TaskRepository repositorio, String id) {
	    boolean encontrou = false;
	    
		for (Task task: repositorio.findAll()) {
			if (task.getId().equals(id)) {
				task.setDone(true);
				repositorio.save(task);
				encontrou = true;
				break;
			}
		}
		
		
		if (encontrou) {
			System.out.println("\nTask alterada com sucesso!\n");
		} else {
			System.out.println("\nNenhuma Task foi encontrada!\n");
		}
		
	}
	
	private void cadastraTask(TaskRepository repositorio,String description, String date) {
		LocalDate localdate = LocalDate.parse(date);
		Task task = new Task(description,localdate);
		repositorio.save(task);
		System.out.println("\nTask cadastrada com sucesso!\n");
	}
		
	

	

}
