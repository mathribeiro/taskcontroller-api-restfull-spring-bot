package br.com.mdsistemas.training.domains.gateways.http.controllers.database.task.mongo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mdsistemas.training.domains.Task;
import br.com.mdsistemas.training.domains.gateways.http.controllers.database.task.TaskDatabaseGateway;
import br.com.mdsistemas.training.domains.gateways.http.controllers.database.task.mongo.repository.TaskRepository;
import br.com.mdsistemas.training.domains.gateways.http.controllers.exceptions.ErrorToAccessDatabaseGatewayException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TaskMongoDatabaseGateway implements TaskDatabaseGateway  {

	@Autowired
	private TaskRepository taskRepository;
	

	public Task create(final Task task) {
		
		try {
			log.trace("task: {}", task);
			
			final Task taskCreated = this.taskRepository.save(task);
			
			return taskCreated;
		} catch (Exception error) {
			log.error("Error: {}", error);
			throw new ErrorToAccessDatabaseGatewayException();
		}
	}


	@Override
	public List<Task> loadTask() {
		return this.taskRepository.findAll();
	}


	@Override
	public void delete(String id) {
		this.taskRepository.deleteById(id);	
	} 


	@Override
	public void alter(final String id, final String description, final LocalDate date, final boolean isDone) {
		// TODO Auto-generated method stub
		for (Task task: taskRepository.findAll()) {
			if (task.getId().equals(id)) {
				task.setDescription(description);
				task.setDate(date);
				task.setDone(true);
				taskRepository.save(task);
				break;
			}
		}
	}


	
}
