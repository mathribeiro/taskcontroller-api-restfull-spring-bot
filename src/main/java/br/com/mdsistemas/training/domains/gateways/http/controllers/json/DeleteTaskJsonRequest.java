package br.com.mdsistemas.training.domains.gateways.http.controllers.json;


import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DeleteTaskJsonRequest {

	@NotBlank
	@Valid
	private String id_task;
	
}
