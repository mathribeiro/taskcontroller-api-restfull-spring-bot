package br.com.mdsistemas.training.domains.gateways.http.controllers.database.task;

import java.time.LocalDate;
import java.util.List;

import br.com.mdsistemas.training.domains.Task;

public interface TaskDatabaseGateway {

	Task create(final Task task);
	List<Task> loadTask();
	void delete(String id);
	void alter(String id, String description, LocalDate date, boolean isDone);
}
