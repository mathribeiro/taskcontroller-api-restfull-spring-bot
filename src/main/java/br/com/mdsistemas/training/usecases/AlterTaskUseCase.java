package br.com.mdsistemas.training.usecases;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mdsistemas.training.domains.gateways.http.controllers.database.task.TaskDatabaseGateway;

@Service
public class AlterTaskUseCase {
	
	@Autowired
	private TaskDatabaseGateway taskDatabaseGateway;

	public void alterTask(final String id, final String description, final LocalDate date, final boolean isDone) {
		this.taskDatabaseGateway.alter(id,description,date,isDone);
	}
}
