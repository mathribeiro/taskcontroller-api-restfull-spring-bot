package br.com.mdsistemas.training.domains.gateways.http.controllers.database.task.mongo.repository;


import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.mdsistemas.training.domains.Task;

public interface TaskRepository extends MongoRepository<Task, String> {
	
    Task findByDescription(String description);
    
	Long countById(String id);

    void deleteById(String id);
    
 
   // List<Task> removeById(String id);

}
